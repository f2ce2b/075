#!/bin/bash --login
bash 01-数据采集/处理逻辑/调试模式.sh "$1"
code=$?; if [ ! $code -eq 0 ];then exit $code; fi

bash 01-数据采集/处理逻辑/调试模式.sh "$1" "${PWD}/10-格式化数据/处理逻辑"
code=$?; if [ ! $code -eq 0 ];then exit $code; fi

bash 01-数据采集/处理逻辑/调试模式.sh "$1" "${PWD}/20-可视化分析/01-Kibana/处理逻辑"
code=$?; if [ ! $code -eq 0 ];then exit $code; fi

# 示例

# 仅更新某一基金某一属性
# bash 66-调试.sh '{"debug_code": "001631", "follow_attributes": ["主要财务指标"]}'

# 更新基金某一属性
# bash 66-调试.sh '{"follow_attributes": ["主要财务指标"]}'

# 跳过基金更新环节
# bash 66-调试.sh '{"debug_code": ""}'
