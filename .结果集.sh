#!/bin/bash --login
target_path='/home/ubuntu/2T/space/资源体系/数据文件/金融/数据存储'

tee "$1" <<EOF
{
  "路径": {
    "项目根路径": "$(realpath $(dirname $(readlink -f $0)))",
    "数据存储":{
      "结构数据": "$target_path/结构数据",
      "应用数据": "$target_path/应用数据",
      "格式数据": "$target_path/格式数据"
    }
  },
  "/": {}
}
EOF