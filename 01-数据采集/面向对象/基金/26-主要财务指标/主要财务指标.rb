# https://fundf10.eastmoney.com/cwzb_001631.html
def 主要财务指标_响应处理(响应)
  返回结果 = (JSON.parse(响应.body) rescue binding.pry)
  binding.pry if (返回结果['ErrCode'] != 0)

  字典 = {
    '期间数据和指标'=> {
      'COMPROFIT' => '本期已实现收益',
      'NETPROFIT' => '本期利润',
      'UNITPROFIT' => '加权平均基金份额本期利润',
      'NGROWTH' => '本期加权平均净值利润率',
      'FNGROWTH' => '本期基金份额净值增长率',
    },
    '期末数据和指标' => {    
      'DISPROFIT' => '期末可供分配利润',
      'DIFUNTIPROFIT' => '期末可供分配基金份额利润',
      'ENDNAV' => '期末基金资产净值',
      'ENDUNITNAV' => '期末基金份额净值',
      'FCNGROWTH' => '基金份额累计净值增长率',
    }
  }
  binding.pry unless 返回结果['Data']['data'].keys == ["FSRQ", 字典.values.map(&:keys)].flatten

  res = {}
  返回结果['Data']['data']['FSRQ'].each_with_index do |日期, 下标|
    res[日期] = {}
    字典.each do |键_1, 值_1|
      res[日期][键_1] = {}
      值_1.each do |键, 值|
        内容 = 返回结果['Data']['data'][键][下标]
        内容 = 内容.gsub(',', '').to_f unless 内容.include?('%')
        res[日期][键_1][值] = 内容
      end
    end
  end

  [true, {'指标' => res, '年份范围' => 返回结果['Data']['years'], '当前年' => 返回结果['Data']['year']}]
end

def 主要财务指标_报告期(参数, 年份范围: [nil], 非循环调用: true)
  年份范围.each do |当前年|
    url = "https://api.fund.eastmoney.com/f10/GetArrayCwzb?fundcode=#{参数['基金代码']}&showtype=0&year=#{当前年}"
    res = 网络请求(请求链接: url)
    binding.pry unless res[0]

    res = 主要财务指标_响应处理(res[1])
    binding.pry unless res[0]
    
    存储文件夹 = File::join([参数['存储路径'], 参数['属性值']['主要财务指标']['存储名'], '报告期'])
    已存在文件数 = 0
    res[1]['指标'].each do |日期, 数据|
      文件夹 = File::join([存储文件夹, 日期[0..3]])
      FileUtils.mkdir_p(文件夹) unless File::directory?(文件夹)
      文件 = File::join([文件夹, 日期[5..9]])
      已存在文件数 += 1 if File::file?(文件)
      File::open(文件, 'w') do |文件|
        文件.write(JSON.pretty_generate(数据))
      end
    end

    # 一年两期 要判定是否存在要大于等于3
    if 已存在文件数 >= 3
      print "[#{Time.now.strftime("%Y-%m-%d %H:%M:%S:%L")}] 跳出循环: 判定后续数据已存在\n"
      return [true, ""]
    end
    
    return 主要财务指标_报告期(参数, 年份范围: res[1]['年份范围']-[res[1]['当前年']], 非循环调用: false) if 非循环调用
  end

  [true, ""]
end

def 主要财务指标_(参数)
  res = 主要财务指标_报告期(参数)
  binding.pry unless res[0]

  [true, ""]
end