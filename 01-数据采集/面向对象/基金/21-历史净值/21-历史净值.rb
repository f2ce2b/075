
def 历史净值_解析响应(响应)
  remove_var = 响应.gsub(/(var\s[a-zA-Z]{1,}\=)(.*)(;$)/, '\2')
  html_content = remove_var.gsub(/(\{\scontent:\")(.*)(\",records.*$)/, '\2')
  html_object = Nokogiri::HTML.parse(html_content)

  datas = [html_object.css('th').map{|children| children.text}]

  datas = datas + html_object.css('tr').map do |children|
    children.css('td').map do |td|
      td.text
    end 
  end 

  datas.delete_if{|array| array == []}

  tmp = {}
  datas[1..-1].each do |array|
    hash = {}
    array.each_with_index do |item, index|
      hash.merge!({datas[0][index] => item})
    end 
    tmp[array[0]] = hash
  end 

  其他信息 = 响应.gsub(/.*records:(\d{1,}),pages:(\d{1,}),curpage:(\d{1,})};/, '\1-\2-\3').split('-').map(&:to_i)

  [true, {"净值信息" => tmp, "数据总数" => 其他信息[0], "总页数"=>其他信息[1], "当前页" => 其他信息[2]}]
end

def 历史净值_获取历史净值信息(当前页:1, 总页数:0, 参数:)
  休眠时间 = rand(6..16)
  print "[#{Time.now.strftime("%Y-%m-%d %H:%M:%S:%L")}] 开始休眠: #{休眠时间}秒......\n"
  sleep 休眠时间
  url = "http://fund.eastmoney.com/f10/F10DataApi.aspx?type=lsjz&code=#{参数['基金代码']}&page=#{当前页}&per=20"
  print "[#{Time.now.strftime("%Y-%m-%d %H:%M:%S:%L")}] 开始请求: [#{当前页}/#{总页数}] #{url}\n"

  response = RestClient.get(url)
  return [false, '响应状态码异常'] if (response.code != 200)

  res = 历史净值_解析响应(response.body)
end

def 历史净值_(参数, 当前页:1, 总页数: 1, 非循环调用: true)

  (当前页..总页数).each do |页数|
    已存在文件数 = 0
    res = 历史净值_获取历史净值信息(参数: 参数, 当前页: 页数, 总页数: 总页数)
    binding.pry unless res[0]
    raise res[1] unless res[0]
    
    res[1]['净值信息'].each do |日期, 数据|
      文件夹 = File::join([参数['存储路径'], 参数['属性值']['历史净值']['存储名'], 日期[0..3]])
      Dir.mkdir(文件夹) unless File::directory?(文件夹)
      文件 = File::join([文件夹, 日期[5..9]])
      if File::file?(文件)
        已存在文件数+=1
        next
      end
      File::open(文件, 'w'){|文件| 文件.write(JSON.pretty_generate(数据))}
    end

    if 已存在文件数 > 1
      print "[#{Time.now.strftime("%Y-%m-%d %H:%M:%S:%L")}] 跳出循环: 判定后续数据已存在\n"
      return [true, ""]
    end

    return 历史净值_(参数, 当前页: res[1]['当前页']+1, 总页数: res[1]['总页数'], 非循环调用: false) if 非循环调用
  end

  [true, ""]
end
