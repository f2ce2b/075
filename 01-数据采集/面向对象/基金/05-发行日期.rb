
def 发行日期_(参数)
  结果 = 参数['解析结果']['发行日期']
  return [false, '解析结果中发行日期为空'] if 结果.nil?

  [true, 结果.gsub(/年|月|日/, '-').split('-').join('-')]
end
