# https://fund.eastmoney.com/Compare/

def 资产配置_响应处理(响应)
  返回结果 = (JSON.parse(响应.body) rescue binding.pry)
  binding.pry if (返回结果['ErrCode'] != 0)

  res = {'资产'=>{},'行业配置'=>{},'前10大持仓'=>{},'前5大债券'=>{}}
  数据 = (JSON.parse(返回结果['Data']) rescue binding.pry)

  资产 = [nil, nil, '份额规模(亿份)', '股票占净比', '债券占净比', '现金占净比', '前10持股集中度', nil]
  zcpz = 数据['zcpz'][0].split(',')
  binding.pry if zcpz.size > 资产.size

  资产.each_with_index do |item, index|
    next if item.nil?
    res['资产'][item] = zcpz[index]||""
  end
    
  行业配置 = ['制造业','金融业','房地产业','信息技术业','农林牧渔业','采掘业','批发零售业','交通运输业','建筑业','社会服务业']
  hypz = 数据['hypz'][0].split(',')
  binding.pry if hypz.size > 行业配置.size

  # 存在如果第5个后面都为空则省略 也就是说前面的逗号是为了站位的
  行业配置.each_with_index do |item, index|
    next if item.nil?
    res['行业配置'][item] = hypz[index]||""
  end
  
  前10大持仓 = 数据['gpcc'][0]
  binding.pry if 前10大持仓.size > 10

  前10大持仓.each_with_index do |持仓, 索引|
    持仓 = 持仓.split(',')
    binding.pry unless 持仓.size == 4
    res['前10大持仓'][索引+1] = {
      "名称" => 持仓[1],
      "代码" => 持仓[0],
      "占比" => 持仓[2],
    }
  end

  数据['zqcc'][0].each_with_index do |持仓, 索引|
    持仓 = 持仓.split(',')
    binding.pry unless 持仓.size == 2
    res['前5大债券'][索引+1] = {
      "名称" => 持仓[0],
      "占比" => 持仓[1],
    }
  end

  [true, res]
end

def 资产配置_(参数)
  url = "https://api.fund.eastmoney.com/FundCompare/ZCPZ?bzdm=#{参数['基金代码']}"
  res = 网络请求(请求链接: url)
  binding.pry unless res[0]

  res = 资产配置_响应处理(res[1])
  binding.pry unless res[0]

  存储文件夹 = File::join([参数['存储路径'], 参数['属性值']['资产配置']['存储名']])
  
  写入文件 = lambda do |日期,数据|
    文件夹 = File::join([存储文件夹, 日期[0..3]])
    FileUtils.mkdir_p(文件夹) unless File::directory?(文件夹)
    文件 = File::join([文件夹, 日期[5..9]])
    File::open(文件, 'w') do |文件|
      文件.write(JSON.pretty_generate(数据))
    end
  end

  近期文件 = Dir["#{存储文件夹}/*/*"][-1]

  if 近期文件
    md5 = `md5sum "#{近期文件}"`
    md5 = md5.split(' ')[0]
    md5_ = Digest::MD5.hexdigest(JSON.pretty_generate(res[1]))
    写入文件.call(Time.now.strftime("%Y-%m-%d"), res[1]) unless md5 == md5_ 
  else
    写入文件.call(Time.now.strftime("%Y-%m-%d"), res[1])  
  end

  [true, ""]
end