def 费用分析_响应处理(响应)
  doc = Nokogiri::HTML.parse(响应.body)
  标准表头 = ['报告期', '费用合计', '管理人报酬', '占比', '托管费', '占比', '交易费', '占比', '销售服务费', '占比']
  binding.pry unless 标准表头 == doc.css('table')[1].css('thead').children[0].children.map(&:text)

  费用数据 = {}
  doc.css('table')[1].css('tbody').css('tr').each do |tr|
    行元素 = tr.css('td').children.map(&:text)
    binding.pry unless 标准表头.size == 行元素.size

    行数据 = 标准表头.map.with_index{|标题, 下标| [(标题 != '占比' ? 标题 : "#{标准表头[下标-1]}-占比"), 行元素[下标]]}.to_h
    行数据['占比']={}
    行数据.each do |键,值|
      next if 值 =~ /\d{4}-\d{2}-\d{2}/
      行数据[键] = 值.gsub(',', '').to_f if 值 =~/[,\.\d]+/
      next unless 键 =~ /-占比/
      行数据['占比'][键.gsub(/-占比/, '')] = 值
      行数据.delete(键)
    end
    费用数据[行数据['报告期'].split('-')[0]] = 行数据
  end

  [true, 费用数据]
end

def 费用分析_(参数)
  url = "https://fundf10.eastmoney.com/fyfx_#{参数['基金代码']}.html"
  res = 网络请求(请求链接: url)
  binding.pry unless res[0]

  res = 费用分析_响应处理(res[1])
  binding.pry unless res[0]

  存储文件夹 = File::join([参数['存储路径'], 参数['属性值']['费用分析']['存储名']])

  res[1].each do |年份, 数据|
    存储路径 = File::join([存储文件夹, 年份])
    File::open(存储路径, 'w') do |文件|
      文件.write(JSON.pretty_generate(数据))
    end
  end

  [true, ""]
end