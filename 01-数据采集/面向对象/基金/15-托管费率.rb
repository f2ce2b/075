
def 托管费率_(参数)
  结果 = 参数['解析结果']['托管费率']
  return [false, '解析结果中托管费率为空'] if 结果.nil?
  # "0.22%（每年）"
  正则 = /([\d .]+)%（每年）/
  return [false, '托管费率 结果组成格式特殊'] unless (结果 =~ 正则)
  [true, {"数值": 结果.gsub(正则, '\1').to_f,"单位":"每年/%"}]
end
