# https://fund.eastmoney.com/Compare/

def 业绩评级_响应处理(响应)
  返回结果 = (JSON.parse(响应.body) rescue binding.pry)
  binding.pry if (返回结果['ErrCode'] != 0)

  res = {'阶段收益'=>{},'历史年度收益'=>{},'定投收益'=>{},'基金评级(3年期)'=>{}}
  数据 = (JSON.parse(返回结果['Data']) rescue binding.pry)

  阶段收益 = [nil,nil,'成立日期','今年来','近1周','近1月','近3月','近6月','近1年','近2年','近3年','近5年','成立来']
  jdsy = 数据['jdsy'][0].split(',')
  binding.pry if jdsy.size > 阶段收益.size

  阶段收益.each_with_index do |item, index|
    next if item.nil?
    res['阶段收益'][item] = jdsy[index]||""
  end
    
  res['历史年度收益'] = 数据['lsndsy'][0]
  
  定投收益 = ['近1年','近2年','近3年','近5年']
  dtsy = 数据['dtsy'][0].split(',')
  binding.pry if dtsy.size > 定投收益.size

  定投收益.each_with_index do |item, index|
    next if item.nil?
    res['定投收益'][item] = dtsy[index]||""
  end

  基金评级3 = ['海通证券',nil,'招商证券','上海证券','济安金信']
  jjpj = 数据['jjpj'][0].split(',')
  binding.pry if jjpj.size > 基金评级3.size

  基金评级3.each_with_index do |item, index|
    next if item.nil?
    评级 = (jjpj[index]||"").split('').size
    res['基金评级(3年期)'][item] = (评级 == 0 ? '' : 评级)
  end

  [true, res]
end

def 业绩评级_(参数)
  url = "https://api.fund.eastmoney.com/FundCompare/YJPJBJ?bzdm=#{参数['基金代码']}"
  res = 网络请求(请求链接: url)
  binding.pry unless res[0]

  res = 业绩评级_响应处理(res[1])
  binding.pry unless res[0]

  存储文件夹 = File::join([参数['存储路径'], 参数['属性值']['业绩评级']['存储名']])
  
  写入文件 = lambda do |日期,数据|
    文件夹 = File::join([存储文件夹, 日期[0..3]])
    FileUtils.mkdir_p(文件夹) unless File::directory?(文件夹)
    文件 = File::join([文件夹, 日期[5..9]])
    File::open(文件, 'w') do |文件|
      文件.write(JSON.pretty_generate(数据))
    end
  end

  近期文件 = Dir["#{存储文件夹}/*/*"][-1]

  if 近期文件
    md5 = `md5sum "#{近期文件}"`
    md5 = md5.split(' ')[0]
    md5_ = Digest::MD5.hexdigest(JSON.pretty_generate(res[1]))
    写入文件.call(Time.now.strftime("%Y-%m-%d"), res[1]) unless md5 == md5_ 
  else
    写入文件.call(Time.now.strftime("%Y-%m-%d"), res[1])  
  end

  [true, ""]
end