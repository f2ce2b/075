# TODO: 所有属性补充
# 说明： 1、报告期时间为半年的，对应的基金换手率为半年度换手率。报告期时间为年的，对应的基金换手率为年度换手率。
# 2、基金换手率用于衡量基金投资组合变化的频率。换手率越高，操作越频繁，越倾向于择时波段操作；基金换手率越低，操作越谨慎，越倾向于买入并持有策略。 

def 换手率_解析响应(响应)
  返回结果 = (JSON.parse(响应.body) rescue binding.pry)
  binding.pry if (返回结果['ErrCode'] != 0)
  res = {}

  返回结果['Data'].each do |换手率|
    binding.pry unless (换手率.keys == ['REPORTDATE', 'STOCKTURNOVER'])
    res[换手率['REPORTDATE']] = {
      "报告日期": 换手率['REPORTDATE'],
      "换手率": 换手率['STOCKTURNOVER']
    }
  end

  [true, {'换手率' => res, '总页数' => (返回结果['TotalCount']/返回结果['PageSize'].to_f).ceil,  '当前页' => 返回结果['PageIndex']}]
end

def 换手率_(参数, 当前页:1, 总页数: 1, 非循环调用: true)
  (当前页..总页数).each do |页数|
    已存在文件数 = 0

    url = "https://api.fund.eastmoney.com/f10/JJHSL?fundcode=#{参数['基金代码']}&pageindex=#{当前页}&pagesize=20"
    res = 网络请求(请求链接: url)
    binding.pry unless res[0]

    res = 换手率_解析响应(res[1])
    binding.pry unless res[0]
    
    (res[1]['换手率'] || {}).each do |日期, 数据|
      文件夹 = File::join([参数['存储路径'], 参数['属性值']['换手率']['存储名'], 日期[0..3]])
      Dir.mkdir(文件夹) unless File::directory?(文件夹)
      文件 = File::join([文件夹, 日期[5..9]])
      if File::file?(文件)
        已存在文件数+=1
        next
      end
      File::open(文件, 'w'){|文件| 文件.write(JSON.pretty_generate(数据))}
    end

    if 已存在文件数 > 1
      print "[#{Time.now.strftime("%Y-%m-%d %H:%M:%S:%L")}] 跳出循环: 判定后续数据已存在\n"
      return [true, ""]
    end

    return 换手率_(参数, 当前页: res[1]['当前页']+1, 总页数: res[1]['总页数'], 非循环调用: false) if 非循环调用
  end

  [true, '']
end