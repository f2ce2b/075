
def 季度涨幅明细_响应处理(响应)

  html_content = 响应.body.gsub(/var apidata={ content:\"(.*)(\"};$)/, '\1')
  html_table = html_content.gsub(/(.*)(\",summary:.*)/, '\1')
  summary = html_content.gsub(/(.*\",summary:\")(.*)/, '\2')

  doc = Nokogiri::HTML.parse(html_table)

  键组 = doc.css('thead').css('tr')[0].children.map(&:text)

  datas = {}
  doc.css('tbody').css('tr').each do |tr|
    年度 = {}
    tr.children.css('td').each_with_index do |td, index|
      年度[键组[index]] = td.text
    end
    datas[年度['时间'].gsub('年', '')] = 年度
  end

  [true, {"总结" => summary, "明细" => datas}]
end

def 季度涨幅明细_(参数)
  url = "https://fundf10.eastmoney.com/FundArchivesDatas.aspx?type=jdndzf&code=#{参数['基金代码']}"
  res = 网络请求(请求链接: url)
  binding.pry unless res[0]

  res = 季度涨幅明细_响应处理(res[1])
  binding.pry unless res[0]

  存储文件夹 = File::join([参数['存储路径'], 参数['属性值']['季度涨幅明细']['存储名']])

  存储路径 = File::join([存储文件夹, '明细'])
  Dir.mkdir(存储路径) unless File::directory?(存储路径)
  res[1]['明细'].each do |年份, 数据|
    File::open(File::join([存储路径, 年份]), 'w') do |文件|
      文件.write(JSON.pretty_generate(数据))
    end
  end
  
  日期 = res[1]['总结'].gsub(/.*(\d{4}-\d{2}-\d{2}).*/, '\1')
  binding.pry if 日期.to_i == 0
  总结文件夹 = File::join([存储文件夹, '总结', 日期[0..3]])
  FileUtils.mkdir_p(总结文件夹) unless File::directory?(总结文件夹)

  总结 = File::open(File::join([总结文件夹, 日期[0..6]]), 'a+'){|文件| JSON.parse(文件.read) rescue [] }
  总结 << res[1]['总结']
  File::open(File::join([总结文件夹, 日期[0..6]]), 'w') do |文件|
    文件.write(JSON.pretty_generate(总结.uniq))
  end

  [true, '']
end