
def 年度涨幅对比_(参数)
  url = "https://fundf10.eastmoney.com/FundArchivesDatas.aspx?type=yearzf&code=#{参数['基金代码']}"
  res = 网络请求(请求链接: url)
  binding.pry unless res[0]

  # 年度与季度数据格式一致
  res = 季度涨幅对比_响应处理(res[1])
  binding.pry unless res[0]

  res[1]['明细'].each do |年度, 数据|
    存储路径 = File::join([参数['存储路径'], 参数['属性值']['年度涨幅对比']['存储名'], "#{年度[0..3]}"])
    # next if File::file?(存储路径)
    File::open(存储路径, 'w') do |文件|
      文件.write(JSON.pretty_generate(数据))
    end
  end

  [true, '']
end