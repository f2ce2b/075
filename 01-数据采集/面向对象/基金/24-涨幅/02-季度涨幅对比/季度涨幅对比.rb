def 季度涨幅对比_响应处理(响应)
  html_content = 响应.body.gsub(/var qapidata={ content:\"(.*)(\"};$)/, '\1')
  doc = Nokogiri::HTML.parse(html_content)
  季度 = doc.css('thead').css('tr')[0].children.map(&:text)
  键组 = doc.css('tbody').css('tr').map{|tr| tr.children.css('td')[0].children[0].text rescue tr.children.css('td')[0].text}.delete_if{|item| item == ''}

  年季 = {}

  (1...季度.size).each do |季度下标|
    (0...键组.size).each do |键组下标|
      # 属性值 = doc.css('tbody').css('tr')[键组下标].children.css('td')[季度下标].text
      属性值 = doc.css('tbody').css('tr')[键组下标].children[季度下标].text
      (年季[季度[季度下标]]||={})[键组[键组下标]] = 属性值
    end
  end

  [true, {"明细" => 年季}]
end

def 季度涨幅对比_(参数)
  url = "https://fundf10.eastmoney.com/FundArchivesDatas.aspx?type=quarterzf&code=#{参数['基金代码']}"
  res = 网络请求(请求链接: url)
  binding.pry unless res[0]

  res = 季度涨幅对比_响应处理(res[1])
  binding.pry unless res[0]

  存储文件夹 = File::join([参数['存储路径'], 参数['属性值']['季度涨幅对比']['存储名']])

  res[1]['明细'].each do |年季, 数据|
    存储路径 = File::join([存储文件夹, "20#{年季[0..1]}"])
    Dir.mkdir(存储路径) unless File::directory?(存储路径)
    File::open(File::join([存储路径, 年季]), 'w') do |文件|
      文件.write(JSON.pretty_generate(数据))
    end
  end

  [true, '']
end