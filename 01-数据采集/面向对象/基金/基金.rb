require 'pry'
require 'json'
require 'fileutils'
require 'uri'
require 'net/http'
require 'nokogiri'
require 'rest-client'

require_relative '../网络/请求.rb'

class Fund
  attr_accessor :fund_code
  attr_accessor :storage_path
  attr_accessor :current_path
  attr_accessor :method_patams
  attr_accessor :ignore_attributes
  attr_accessor :follow_attributes

  def initialize(**params)
    self.fund_code = params[:fund_code]
    self.storage_path = params[:storage_path]    
    self.current_path = File.expand_path(File.dirname(__FILE__))
    self.ignore_attributes = params[:ignore_attributes] || []
    self.follow_attributes = params[:follow_attributes]

    Dir.foreach(self.current_path).sort.each do |filename|
      next unless filename =~ /^\d{2}\-(.*)/

      属性 = {
        # "属性名" => 属性名,
        "记录值" => params[filename.gsub('.rb', '').to_sym],
        "当前值" => nil,
        # "存储路径" => File::join([params[:storage_path], filename.gsub('.rb', '')]),
        "存储名" => filename.gsub('.rb', ''),
        "存储类型" => "文件",
      }

      文件路径 = File::join([self.current_path, filename])
      if File::file?(文件路径)
        属性名 = filename.gsub(/^\d{2}\-(.*)\.rb$/,'\1')
      elsif Dir["#{文件路径}/*.rb"][0]
        属性名 = filename.gsub(/^\d{2}\-(.*)/,'\1')
        文件路径 = Dir["#{文件路径}/*#{属性名}.rb"][0]
        属性["存储类型"] = '目录'
      elsif Dir["#{文件路径}/*/*.rb"][0]
        Dir["#{文件路径}/*/*.rb"].each do |路径|
          路径_数组 = 路径.split('/')
          属性 = {
            "记录值" => params[路径_数组[-1].gsub('.rb', '').to_sym],
            "当前值" => nil,
            "存储名" => 路径_数组[-3..-2].join('/'),
            "存储类型" => "目录",
          }

          属性名 = 路径_数组[-2].gsub(/^\d{2}\-(.*)/,'\1')
          self.class.attr_accessor "#{属性名}"
          require 路径
          self.instance_variable_set("@#{属性名}", 属性)
        end

        next
      else
        binding.pry
      end

      self.class.attr_accessor "#{属性名}"
      require 文件路径
      self.instance_variable_set("@#{属性名}", 属性)
    end

  end

  def 方法参数
    属性值 = {}
    self.instance_variables.each do |属性名|
      next if 属性名.to_s =~/^@[a-z A-z]+.*/
      属性值["#{属性名.to_s.gsub('@', '')}"] = self.instance_variable_get(属性名)
    end

    if self.method_patams
      self.method_patams['属性值'] = 属性值
      return self.method_patams 
    end

    self.method_patams = {
      # "解析对象" => 解析对象,
      "基金代码" => fund_code,
      "存储路径" => self.storage_path,
      "属性值" => 属性值,
    }

    return self.method_patams if self.follow_attributes

    url = "http://fundf10.eastmoney.com/jbgk_#{self.fund_code}.html"
    print "[#{Time.now.strftime("%Y-%m-%d %H:%M:%S:%L")}] 开始请求: #{url}\n"
    url = URI(url)
    http = Net::HTTP.new(url.host, url.port);
    request = Net::HTTP::Get.new(url)
    request["Cookie"] = "ASP.NET_SessionId=lec4ugwxvplxmxojuctehgcd"
    response = http.request(request)

    解析对象 = Nokogiri::HTML.parse(response.read_body)
    解析结果 = {}
    解析对象.xpath("//*[@class='detail']").css('tr').each do |tr|
      th_contents = tr.css('th').map(&:text)
      td_contents = tr.css('td').map(&:text)
      tmp = {}
      th_contents.each_with_index do |th_content, index|
        tmp.merge!({th_content => td_contents[index]})
      end 
      解析结果.merge!(tmp)
    end 

    binding.pry if 解析结果 == {}
    self.method_patams['解析结果'] = 解析结果
    self.method_patams
  end

  def 数据结构更新
    属性值数组 = self.instance_variables.map do |属性名|
      next if 属性名.to_s =~/^@[a-z A-z]+.*/
      self.instance_variable_get(属性名)
    end
    属性值数组.delete_if{|属性值| 属性值.nil?}
    存储名数组 = 属性值数组.map{|属性值| 属性值['存储名'].split('/')}.flatten

    Dir.foreach(self.storage_path).sort.each do |存储名|
      next if ['.', '..'].include?(存储名)
      next if 存储名数组.include?(存储名)
      
      存储路径 = File::join([self.storage_path, 存储名])
      重命名 = 存储名数组.select{|name| name =~ Regexp.new("\\d{2}-#{存储名.split('-')[1]}$") }
      
      if 重命名[0]
        binding.pry
        File.rename(存储路径, File::join([self.storage_path, 重命名]))
      else
        if File::directory?(存储路径)
          binding.pry
          FileUtils.rm_r(存储路径)
        elsif File::file?(存储路径)
          binding.pry
          File.delete(存储路径)
        end
      end
    end

    属性值数组.each do |属性值|
      文件路径 = File::join([self.storage_path, 属性值['存储名']])
      if (属性值['存储类型'] == '文件')
        File::open(文件路径, 'w'){|文件| } unless File::file?(文件路径)
      elsif (属性值['存储类型'] == '目录')
        FileUtils.mkdir_p(文件路径) unless File::directory?(文件路径)
      end      
    end

  end

  def 更新
    self.数据结构更新

    # 执行

    self.instance_variables.map do |属性名|
      next if 属性名.to_s =~/^@[a-z A-z]+.*/
      属性值 = self.instance_variable_get(属性名)

      方法名 = 属性名.to_s.gsub('@', '')
      next if self.ignore_attributes.include?(方法名)
      next if self.follow_attributes && !self.follow_attributes.include?(方法名) 

      res = self.send("#{方法名}_", self.方法参数)
      binding.pry unless (res ||= [false, '返回结果为空'])[0]

      next if (属性值['存储类型'] == '目录')

      属性值['当前值'] = res[1]
      self.instance_variable_set(属性名, 属性值)

      # storage = File::join([self.current_path, storage_name])
      # if File::file?(storage)
      #   属性名 = "@#{storage_name.gsub(/^\d{2}\-(.*)\.rb$/, '\1')}"

        
      # elsif File::directory?(storage)
      #   方法名 = storage_name.gsub(/^\d{2}\-(.*)/, '\1')
      #   next if self.ignore_attributes.include?(方法名)
      #   next if self.follow_attributes && !self.follow_attributes.include?(方法名) 
      #   binding.pry if 方法名 == '涨幅'

      #   res = self.send("#{方法名}_", self.方法参数)
      #   binding.pry unless (res ||= [false, '返回结果为空'])[0]
      # end
    end

    # 更新
    self.instance_variables.each do |属性名|
      next if 属性名.to_s =~/^@[a-z A-z]+.*/
      方法名 = 属性名.to_s.gsub('@', '')
      next if self.follow_attributes && !self.follow_attributes.include?(方法名) 

      属性值 = self.instance_variable_get(属性名)
      next if 属性值['存储类型'] == '目录'
      next if 属性值['记录值'] == 属性值['当前值']
      # [属性名, 属性值['记录值'], 属性值['当前值']]
      case 属性值['当前值']
      when Hash
        next if (JSON.parse(属性值['记录值']) rescue {}) == 属性值['当前值']
      end

      if (属性值['存储类型'] == '文件')
        File::open(File::join([self.storage_path, 属性值['存储名']]), 'w') do |文件|
          case 属性值['当前值']
          when Hash, Array
            文件.write(JSON.pretty_generate(属性值['当前值']))
          else
            文件.write(属性值['当前值'])
          end
        end
      end

    end

  end

end