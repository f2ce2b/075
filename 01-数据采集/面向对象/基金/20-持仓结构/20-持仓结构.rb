def 持仓结构_返回结构解析(table_conent:)
  titles_ = table_conent.css('thead')[0].css('tr')[0].css('th').map{|children| children.text}
  items = ["股票代码", "股票名称", "占净值比例", "持股数（万股）", "持仓市值（万元）"]
  get_indexs = items.map{|item|titles_.index(item)}
  titles = get_indexs.map{|index| titles_.at(index)}

  tmp = {}
  table_conent.css('tbody')[0].css('tr').each do |tr_content|
    
    array = tr_content.css('td').map{|children| children.text}
    contents = get_indexs.map{|index| array.at(index).gsub(',', '')}
    tmp_ = {}
    titles.each_with_index do |title, index|
      case title
      when /持股数（万股）/
        tmp_['持股数'] = {
          "数值" => contents[index].to_f,
          "单位" => '万股'
        }
      when /持仓市值（万元）/
        tmp_['持仓市值'] = {
          "数值" => contents[index].to_f,
          "单位" => '万元'
        }
      else
        tmp_.merge!(title => contents[index])
      end 
    end 

    tmp[contents[1]] = tmp_
  end 

  [true, tmp]
end

def 持仓结构_获取年度持仓信息(参数, 当前年份:)
  url = "http://fundf10.eastmoney.com/FundArchivesDatas.aspx?type=jjcc&code=#{参数['基金代码']}&topline=1000&year=#{当前年份}&month=3,6,9,12&rt=0.3260991934481672"
  res = 网络请求(请求链接: url)
  res[1].body rescue binding.pry

  html_content = res[1].body.gsub(/var apidata={ content:\"(.*)(\",arryear.*$)/, '\1')
  return [false, '返回结果为空'] if html_content == ''
  html_object = Nokogiri::HTML.parse(html_content)

  datas = {}
  html_object.css('table').each_with_index do |table, index|
    h4 = html_object.css('h4')[index]
    key = h4.css('label')[-1].text.gsub(/.*(\d{4}\-\d{2}\-\d{2})/, '\1')
    res = 持仓结构_返回结构解析(table_conent: table)
    datas[key] = {
      "标题" => h4.css('label')[0].text.gsub('  ', '-'),
      "构成" => res[1]
    }
  end

  [true, datas]
end

def 持仓结构_(参数)
  return [false, '持仓结构处理失败 基金类型为空'] if 参数['属性值']['基金类型'].nil?
  return [false, '持仓结构处理失败 基金类型为空'] if (基金类型 = 参数['属性值']['基金类型']['当前值']).nil?
  return [true, "债券类型持仓结构数据暂无"] if (基金类型 =~ /.*债券.*/)

  return [false, '持仓结构处理失败 发行日期为空'] if 参数['属性值']['发行日期'].nil?
  return [false, '持仓结构处理失败 发行日期为空'] if (发行日期 = 参数['属性值']['发行日期']['当前值']).nil?
  
  # 存在例如15年10月成立的基金没有15年的持仓信息
  发行日期 = 发行日期.split('-')
  起始年份 = 发行日期[0].to_i
  起始年份+= 1 if 发行日期[1].to_i >= 10

  记录值 = (参数['属性值']['持仓结构']['记录值'] || {})

  (起始年份 .. Time.now.year).each do |年份|
    next if (!记录值[年份.to_s].nil?) && (记录值[年份.to_s] != {}) && (Time.now.year != 年份)

    存储路径 = File::join([参数['存储路径'], 参数['属性值']['持仓结构']['存储名'], 年份.to_s])
    Dir.mkdir(存储路径) unless File::directory?(存储路径)
    res = 持仓结构_获取年度持仓信息(参数, 当前年份:年份)

    binding.pry unless res[0]

    res[1].each do |时间, 数据|
      File::open(File::join([存储路径, 时间]), 'w') do |文件|
        文件.write(JSON.pretty_generate(数据))
      end
    end

  end

  return [true, ""]
end
