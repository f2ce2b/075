
def 基金评级_解析响应(响应)
  返回结果 = (JSON.parse(响应.body) rescue binding.pry)
  binding.pry if (返回结果['ErrCode'] != 0)

  字典 = {
    'FCODE'=>'基金代码',
    'RDATE'=>'评级日期',
    'HTPJ'=>'HTPJ',
    'ZSPJ'=>'招商评级',
    'SZPJ3'=>'上海证券评级-三年期',
    'ZSPJ5'=>'上海证券评级-五年期',
    'JAPJ'=>'济安金信评级'
  }

  res = {}

  返回结果['Data'].each do |评级|
    binding.pry unless (字典.keys == 评级.keys)
    机构评级 = 评级.select{|k,v| (!['FCODE', 'RDATE'].include?(k) && !['', nil].include?(v))}
    next if 机构评级 == {}
    (res[评级['FCODE']]||={})[评级['RDATE']] = 机构评级.map{|k,v| [字典[k], v.to_i]}.to_h
  end
  
  [true, {'评级' => res, '总页数' => (返回结果['TotalCount']/返回结果['PageSize'].to_f).ceil,  '当前页' => 返回结果['PageIndex']}]
end

def 基金评级_(参数, 当前页:1, 总页数: 1, 非循环调用: true)
  (当前页..总页数).each do |页数|
    已存在文件数 = 0
    
    url = "https://api.fund.eastmoney.com/F10/JJPJ?fundcode=#{参数['基金代码']}&pageIndex=#{当前页}&pageSize=50"
    res = 网络请求(请求链接: url)
    binding.pry unless res[0]

    res = 基金评级_解析响应(res[1])
    binding.pry unless res[0]
    
    (res[1]['评级'][参数['基金代码']] || {}).each do |日期, 数据|
      文件夹 = File::join([参数['存储路径'], 参数['属性值']['基金评级']['存储名'], 日期[0..3]])
      Dir.mkdir(文件夹) unless File::directory?(文件夹)
      文件 = File::join([文件夹, 日期[5..9]])
      if File::file?(文件)
        已存在文件数+=1
        next
      end
      File::open(文件, 'w'){|文件| 文件.write(JSON.pretty_generate(数据))}
    end

    if 已存在文件数 > 1
      print "[#{Time.now.strftime("%Y-%m-%d %H:%M:%S:%L")}] 跳出循环: 判定后续数据已存在\n"
      return [true, ""]
    end

    return 基金评级_(参数, 当前页: res[1]['当前页']+1, 总页数: res[1]['总页数'], 非循环调用: false) if 非循环调用
  end

  return [true, ""]
end