tt_qequest_file = Dir["#{Dir.pwd}/**/[0-9]*[0-9]*-初始化/[0-9]*-请求参数/[0-9]*-天天基金.json"][0]
TT_RequestParams = File.open(tt_qequest_file){|file| JSON.parse(file.read)}

def 网络请求(请求链接:, 请求方法: "Get", 请求超时: nil, 请求头: nil, 请求体: {}, 重试次数: 10, 输出日志: lambda{|参数| print "[#{Time.now.strftime("%Y-%m-%d %H:%M:%S:%L")}] 开始请求: #{参数}\n"})

  请求参数 = {
    'url' => 请求链接, 
    'method' => 请求方法, 
    'headers' => 请求头, 
    'cookies' => nil, 
    'payload' => 请求体, 
    'timeout' => 请求超时, 
    'open_timeout' => 请求超时
  }

  请求参数.merge!(TT_RequestParams)

  输出日志.call(请求链接)
  begin
    response = RestClient::Request.execute(**Hash[请求参数.map{|(k,v)| [k.to_sym,v]}])
  rescue Errno::ECONNRESET
    args = method(__method__).parameters.map do |arg| 
      name = arg[1].to_s
      [arg[1], binding.local_variable_get(name)]
    end.to_h
    args[:重试次数] -= 1
    binding.pry unless args[:重试次数] > 0

    休眠时间 = rand(6..16)
    print "[#{Time.now.strftime("%Y-%m-%d %H:%M:%S:%L")}] 请求失败: #{休眠时间}秒后再次尝试......\n"
    sleep 休眠时间
    return self.send(__method__, **args)
  rescue => e
    binding.pry
  end

  return [false, '响应状态码异常'] if (response.code != 200)
  [true, response]
end