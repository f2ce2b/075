let JsonRW = require("../00-初始化/20-编码语言/Nodejs/读写JSON对象.js")

let argv1 = new JsonRW({文件绝对路径: process.argv.slice(2)[0]})

let 格式化数据 = {}

let iteration = (object, format) => {
  for (let [key, value] of Object.entries(object)) {
    format_key = key.replace(/^\d{2}\-([\u4e00-\u9fa5 A-Z a-z \( \)]+)$/, "$1")
    switch (typeof value){
      case 'object':
        iteration(value, (format[format_key] = {}))
        break;
      case 'string':
        try {
          format[format_key] = JSON.parse(value)
        } catch (error) {
          format[format_key] = value          
        }
        break;
    }
    
  }
}

iteration(Object.entries(argv1.文件内容)[0][1], 格式化数据)

argv1.更新(格式化数据)
// console.log(JSON.stringify(格式化数据, null, 2))
