require_relative '../../00-初始化/20-编码语言/Ruby/结果集.rb'
require_relative '../../../../01-数据采集/面向对象/基金/基金.rb'

结果集 = ResultFile.new({文件路径: ARGV[0]})
运行参数 = JSON.parse(['', nil].include?(ARGV[1]) ? '{}' : ARGV[1])

结构路径 = 结果集.内容.keys[0]
Dir["#{结构路径}/**/[0-9]*-基金/00-代码库/*"].each do |链接路径|
  代码 = 链接路径.split('/')[-1]
  next if 运行参数['debug_code'] && 运行参数['debug_code'] != 代码

  相对路径_链接 = 链接路径.gsub(结构路径+'/', '').split('/')
  相对路径_存储 = 结果集.内容[结构路径].dig(*相对路径_链接).split('/')
  存储内容 = 结果集.内容[结构路径].dig(*相对路径_存储)

  参数 = 运行参数.clone
  参数.merge!({
    fund_code: 代码,
    storage_path: 链接路径,
  })

  参数.merge!(存储内容)
  基金 = Fund.new(**Hash[参数.map{|(k,v)| [k.to_sym,v]}])
  基金.更新
end

# exit 255
