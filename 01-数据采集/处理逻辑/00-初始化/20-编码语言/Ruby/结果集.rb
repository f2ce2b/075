require 'pry'
require 'json'

class ResultFile 
  attr_accessor :文件路径
  attr_accessor :内容

  def initialize(**params)
    default = {
    }

    params = default.merge!(params)
    params.each do |key, value|
      self.instance_variable_set("@#{key}", value)
    end

    File::open(self.文件路径) do |文件对象|
      self.内容 = (JSON.parse(文件对象.read) rescue {})
      # self.内容 = (JSON.parse(文件对象.read, object_class: OpenStruct) rescue OpenStruct.new)
    end

  end

  def 更新(content=self.内容)
    File::open(self.文件路径, 'w') do |文件对象|
      文件对象.write(JSON.pretty_generate(content))
    end
  end
end