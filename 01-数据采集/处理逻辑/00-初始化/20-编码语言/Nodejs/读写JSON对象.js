fs = require('fs')

class JsonRW {
  constructor(params = {}) {
    let defaultValue = {
      文件绝对路径: undefined,
      文件内容: {}
    }

    params = Object.assign(defaultValue, params)

    for (let [key, value] of Object.entries(params)) {
      this[key] = value
    }

    let data
    if (fs.existsSync(this.文件绝对路径)){
      data = fs.readFileSync(this.文件绝对路径, 'utf8')
    }else{
      data = '{}'
      fs.writeFileSync(this.文件绝对路径, data);
    }

    try {
      this.文件内容 = JSON.parse(data)
    } catch (error) {
      this.文件内容 = {}
    }
  }

  读取() {
    return this.文件内容
  }

  添加(key = null, value = null) {
    // let tmp = {}
    // tmp[key] = value
    // this.文件内容 = Object.assign(this.文件内容, tmp)
    this.文件内容[key] = value
    this.update()
  }

  更新(result = this.文件内容) {
    fs.writeFileSync(this.文件绝对路径, JSON.stringify(result, null, 2))
  }

}

module.exports = JsonRW;
