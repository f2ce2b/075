#!/bin/bash --login

if [ -z "$2" ];then
  TOP=$(dirname $(readlink -f $0))
  source "$TOP/00-初始化/10-配置文件/01-路径配置.sh"
else
  TOP=$2
fi

debug_file_path="$TOP/.调试模式.sh"
echo '#!/bin/bash --login' > "$debug_file_path"

function exec (){
  if [ -f "$1" ]; then
    echo 'printf "[$(date '"'+%Y-%m-%d %H:%M:%S:%3N'"')] "' >> $debug_file_path
    suffix=${1##*.}
    if [[ "$suffix" == 'sh' ]]; then 
      echo "echo '开始加载: ${1/"$TOP/"/''}'" >> $debug_file_path
      echo "source '$1' '$TOP/99-结果集.json' '$2'" >> $debug_file_path
    elif [[ "$suffix" == 'js' ]]; then 
      echo "echo '开始执行: ${1/"$TOP/"/''}'" >> $debug_file_path
      echo "node '$1' '$TOP/99-结果集.json' '$2'" >> $debug_file_path
      echo 'code=$?; if [ ! $code -eq 0 ];then echo '"'[false, \"${1/"$TOP/"/''} 状态码异常\"]'"'; exit $code; fi' >> $debug_file_path
    elif [[ "$suffix" == 'rb' ]]; then 
      echo "echo '开始执行: ${1/"$TOP/"/''}'" >> $debug_file_path
      echo "ruby '$1' '$TOP/99-结果集.json' '$2'" >> $debug_file_path
      echo 'code=$?; if [ ! $code -eq 0 ];then echo '"'[false, \"${1/"$TOP/"/''} 状态码异常\"]'"'; exit $code; fi' >> $debug_file_path
    elif [[ "$suffix" == 'gitkeep' ]]; then 
      echo 'echo ""' >> $debug_file_path
      return 0;
    elif [[ "$suffix" == 'json' ]]; then 
      echo 'echo ""' >> $debug_file_path
      return 0;
    else
      echo '[false, "尚未定义'$suffix'后缀相关处理方法"]'
      exit 255
    fi
  # 匹配软连接目录
  elif [ -L "$1" ]&&[ -d "$1" ]; then
    real_path="$(ls -al "$1" | awk '{print $NF}')"
    find $real_path -maxdepth 1 -print0 | sort -z | while IFS= read -r -d $'\0' target; do
      if [[ "$target" == "$real_path" ]]; then continue;fi
      exec "$target" "$2"
      code=$?; if [ ! $code -eq 0 ];then return $code; fi
    done
  elif [ -d "$1" ]; then
    find "$1" -maxdepth 1 -print0 | sort -z | while IFS= read -r -d $'\0' target; do
      if [[ "$target" == "$1" ]]; then continue;fi
      exec "$target" "$2"
      code=$?; if [ ! $code -eq 0 ];then return $code; fi
    done
  fi
}

find $TOP -maxdepth 1 -type d -print0 | sort -z | while IFS= read -r -d $'\0' dir; do
  if [[ "$dir" == "$TOP" ]]; then continue;fi
  exec "$dir" "$1"
  code=$?; if [ ! $code -eq 0 ];then exit $code; fi
done

code=$?; if [ ! $code -eq 0 ];then echo '[false, "状态码异常"]'; exit $code; fi
echo 'echo '"'[true, \"\"]'"'' >> $debug_file_path
# exit 0
source "$debug_file_path"

# bash 66-调试.sh '{"debug_code": "001631", "ignore_attributes": ["持仓结构", "历史净值"]}'
# bash 66-调试.sh '{"ignore_attributes": ["持仓结构", "历史净值"]}'
# bash 66-调试.sh '{"debug_code": "001631", "follow_attributes": ["换手率"]}'
# bash 66-调试.sh '{"debug_code": ""}'