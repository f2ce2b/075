#!/bin/bash --login
TOP=$(dirname $(readlink -f $0))

source "$TOP/00-初始化/10-配置文件/01-路径配置.sh"

function exec (){
  if [ -f "$1" ]; then
    printf "[$(date '+%Y-%m-%d %H:%M:%S:%3N')] "
    suffix=${1##*.}
    if [[ "$suffix" == 'sh' ]]; then 
      echo "开始加载: ${1/"$TOP/"/''}"
      source "$1" "$TOP/99-结果集.json"
    elif [[ "$suffix" == 'js' ]]; then 
      echo "开始执行: ${1/"$TOP/"/''}"
      node "$1" "$TOP/99-结果集.json"
      code=$?; if [ ! $code -eq 0 ];then return $code; fi
    elif [[ "$suffix" == 'rb' ]]; then 
      echo "开始执行: ${1/"$TOP/"/''}"
      ruby "$1" "$TOP/99-结果集.json"
      code=$?; if [ ! $code -eq 0 ];then return $code; fi
    elif [[ "$suffix" == 'gitkeep' ]]; then 
      echo "跳过文件: ${1/"$TOP/"/''}"
      return 0;
    else
      echo "开始解析: ${1/"$TOP/"/''}"
      echo '[false, "尚未定义'$suffix'后缀相关处理方法"]'
      exit 255
    fi
  # 匹配软连接目录
  elif [ -L "$1" ]&&[ -d "$1" ]; then
    real_path="$(ls -al "$1" | awk '{print $NF}')"
    find $real_path -maxdepth 1 -print0 | sort -z | while IFS= read -r -d $'\0' target; do
      if [[ "$target" == "$real_path" ]]; then continue;fi
      exec "$target"
      code=$?; if [ ! $code -eq 0 ];then return $code; fi
    done
  elif [ -d "$1" ]; then
    find "$1" -maxdepth 1 -print0 | sort -z | while IFS= read -r -d $'\0' target; do
      if [[ "$target" == "$1" ]]; then continue;fi
      exec "$target"
      code=$?; if [ ! $code -eq 0 ];then return $code; fi
    done
  fi
}

find $TOP -maxdepth 1 -type d -print0 | sort -z | while IFS= read -r -d $'\0' dir; do
  if [[ "$dir" == "$TOP" ]]; then continue;fi
  exec $dir
  code=$?; if [ ! $code -eq 0 ];then exit $code; fi
done

code=$?; if [ ! $code -eq 0 ];then echo '[false, "状态码异常"]'; exit $code; fi
echo '[true, ""]'