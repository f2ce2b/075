#!/bin/bash --login

if [ -f "$1" ]||[ -L "$1" ]; then rm $1; fi
touch $1

# 为每个根目录下的代码创建软链接
find $PATH_DATA_STRUCTURE -maxdepth 1 -type d -print0 | while IFS= read -r -d $'\0' root_dir; do
  if [[ "$root_dir" == "$PATH_DATA_STRUCTURE" ]]; then continue;fi
  code_dir="$root_dir/00-代码库"
  if [ ! -d "$code_dir" ]; then continue; fi

  # 删除已失效的链接文件
  find "$code_dir" -xtype l -delete

  # find 正则无法使用 [0-9]\{6\}
  find $root_dir -regex ".*/\([0-9][0-9][0-9][0-9][0-9]+\|[A-Z]+\:[0-9]+\)" -type d -print0 | while IFS= read -r -d $'\0' child_dir; do
    if [ ! -L "$code_dir/${child_dir##*/}" ]; then ln -s $child_dir $code_dir; fi
  done

done

tree -J $PATH_DATA_STRUCTURE > $1

# exit 255