fs = require('fs')

let JsonRW = require("../00-初始化/20-编码语言/Nodejs/读写JSON对象.js")

let argv1 = new JsonRW({文件绝对路径: process.argv.slice(2)[0]})

let 格式化结构 = {}

let 递归查询 = (contents, keys) => {
  for (let content of contents){
    if (content['name'] == keys[0] && keys.length == 1){
      return content
    }else if (content['name'] == keys[0]){
      keys.splice(0, 1)
      return 递归查询(content['contents'], keys)
    }
  }
}

let iteration = (object, format, path=[]) => {
  if (object['type'] == 'directory' && object['contents'].length > 0){
    for (let kv of object['contents']){
      iteration(kv, (format[object['name']] ||= {}), [path, object['name']].flat())
    }
  }else if (object['type'] == 'directory'){
    format[object['name']] = {}
  }else if (object['type'] == 'file'){
    format[object['name']] = fs.readFileSync([path, object['name']].flat().join('/'), 'utf8')
  }else if (object['type'] == 'link'){
    format[object['name']] = object['target'].replace(new RegExp(argv1.文件内容[0]['name']+'/'),'')

    // keys = object['target'].replace(new RegExp(argv1.文件内容[0]['name']+'/'),'').split('/')
    // contents = argv1.文件内容[0]['contents']
    // target_path = object['target'].split('/')
    // target = 递归查询(contents, keys)
    // target_path.pop()
    // iteration(target, (format||= {}), target_path)
  }
}

iteration(argv1.文件内容[0], 格式化结构)

argv1.更新(格式化结构)
// console.log(JSON.stringify(格式化结构, null, 2))

// process.exit(255)