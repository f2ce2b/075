module SuoYinMoShi
  def 所有索引模式(查询数量: 1000)
    url = self.链接_es("/.kibana/_search?q=type:index-pattern&size=#{查询数量}")
    res = self.请求(链接: url)
    binding.pry unless res[0]
    binding.pry if res[1]['hits']['total']['value'] > 查询数量
    result = {}

    res[1]['hits']['hits'].each do |detail|
      _source = detail['_source']
      
      result[_source['index-pattern']['title']] = {
        '_id': detail['_id']
      }
    end

    result
  end

  def 创建索引模式(标题:)
    参数 = {
      请求方法: 'POST', 
      链接: self.链接_kib('/api/index_patterns/index_pattern'),
      请求头: {
        'kbn-xsrf': true,
        'content-type' => 'application/json'
      }, 
      请求体: {
        "override": false,
        "refresh_fields": true,
        "index_pattern": {
          "title": 标题
        }
      }
    }
    
    res = self.请求(**参数)
    binding.pry unless res[0]
    res
  end

  def 同步索引模式
    索引模式 = self.所有索引模式.keys
    索引别名 = self.全部索引别名
    缺失模式 = 索引别名 - 索引模式
    
    缺失模式.each do |模式名|
      self.创建索引模式(标题: 模式名)
    end

  end
end