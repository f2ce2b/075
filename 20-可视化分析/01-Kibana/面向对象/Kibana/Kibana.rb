require_relative '../../../../10-格式化数据/面向对象/Dobject.rb'

class Kibana < Dobject
  def initialize(**params)
    current_path = File.expand_path(File.dirname(__FILE__))

    params[:属性] = [
      "#{current_path}/../../../../10-格式化数据/面向对象/ES/属性",
      "#{current_path}/属性"
    ]

    params[:方法] = [
      "#{current_path}/../../../../10-格式化数据/面向对象/ES/方法",
      "#{current_path}/方法"
    ]

    super(**params)
    res = self.请求(链接: self.链接_kib)
    raise "Kib服务自检失败!" unless res[0]
  end
end