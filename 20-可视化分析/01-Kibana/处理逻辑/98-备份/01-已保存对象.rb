require_relative '../../面向对象/Kibana/Kibana.rb'

kib = Kibana.new()

参数 = {
  请求方法: 'POST', 
  链接: kib.链接_kib('/api/saved_objects/_export'),
  请求头: {
    'kbn-xsrf': true,
    'content-type' => 'application/json'
  }, 
  请求体: {
    'type': ['dashboard']
  }
}

res = kib.请求(**参数)

结果集 = ResultFile.new({文件路径: ARGV[0]})
文件名 = "#{Digest::MD5.hexdigest(res[1])}.ndjson"

(((结果集.内容['/']['Kibana']||={})['环境配置']||={})['仪表盘']||={})[文件名] = res[1]

结果集.更新
