require 'pry'
require_relative '../../../../01-数据采集/处理逻辑/00-初始化/20-编码语言/Ruby/结果集.rb'
require_relative '../../../面向对象/ES/ES.rb'

结果集 = ResultFile.new({文件路径: ARGV[0]})

Es = ES.new()

def 迭代创建(哈希:, 键组: [])

  哈希.each do |键, 值|
    case 值
    when Hash
      迭代创建(哈希: 值, 键组: [键组, 键].flatten)
    when String
      索引名 = [键组, 键].flatten.join('_')
      索引定义 = JSON.parse(值)

      res = Es.自动构建(映射: 索引定义['映射'], 文档: 索引定义['文档'], 主索引名: 索引名)
      binding.pry unless res[0]
    end
  end
end

结果集.内容['/']['ES文档'].each do |键, 值|
  迭代创建(哈希:值, 键组: [键])
end