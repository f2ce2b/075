require_relative '../../../面向对象/ES/ES.rb'

结果集 = ResultFile.new({文件路径: ARGV[0]})

Es = ES.new()

迭代子索引名 = lambda do |对象:, 键组:[], 结果: []|
  对象.each do |键, 值|
    case 值
    when Hash
      迭代子索引名.call(对象: 值, 键组: [键组, 键].flatten, 结果: 结果)
    when String
      # binding.pry
      结果 << [键组, 键].flatten.join('_')
    end
  end
  结果
end

# 删除非活跃索引
全部索引 = Es.全部索引
使用中索引 = 迭代子索引名.call(对象: 结果集.内容['/']['ES文档'])
结果集.内容['/']['ES文档'].each do |键, 值|
  相关索引 = 全部索引.select{|item| item.include?("#{键}_") }
  非活跃索引 = 相关索引.select{|item| !使用中索引.include?(item.split('_')[0...-2].join('_')) }
  # 非活跃索引 = 相关索引

  非活跃索引.each do |名称|
    # 删除索引也会删除关联别名
    Es.删除索引(名称: 名称)
  end
end
