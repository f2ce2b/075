require_relative '../../../../../面向对象/基金/代码库.rb'
require_relative '../../../../../面向对象/基金/基金.rb'
require_relative '../../../../../../01-数据采集/处理逻辑/00-初始化/20-编码语言/Ruby/结果集.rb'
require_relative '../../../../../面向对象/ES/ES.rb'

结果集 = ResultFile.new({文件路径: ARGV[0]})
es = ES.new()

基金对象 = {}
FundLib.new().文件夹.each do |代码, 路径|
  fund = Fund.new(fund_code: 代码, storage_path: 路径)
  next if fund.基金评级 == {}
  基金对象[代码] = fund
end

基金评级 = {}
基金对象.each do |代码, 对象|
  对象.基金评级.each do |时间,内容_|
    内容 = 内容_.call
    内容.each do |评级机构, 评级分数|
      文档 = {
        '基金简称': 对象.基金简称,
        '基金代码': 代码,
        '报告日期': 时间,
        '评级机构': 评级机构,
        '评级分数': 评级分数,
      }
      基金评级[Digest::MD5.hexdigest(JSON.pretty_generate(文档))] = 文档
    end
  end
end

历史评级定义 = {
  "基金简称": {
    "类型" => "keyword"
  },
  "基金代码": {
    "类型" => "keyword"
  },
  "报告日期": {
    "类型": "keyword",
  },
  "评级机构": {
    "类型" => "keyword"
  },
  "评级分数": {
    "类型" => "integer"
  }
}

历史评级 = es.生成索引定义及文档(定义: 历史评级定义, 目标对象: 基金评级)

(((结果集.内容['/']['ES文档']||={})['基金']||={})['基金评级']||={})['历史评级'] = 历史评级.to_json

结果集.更新