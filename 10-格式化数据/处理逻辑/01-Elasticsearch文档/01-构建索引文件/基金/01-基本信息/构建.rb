require_relative '../../../../../面向对象/基金/代码库.rb'
require_relative '../../../../../面向对象/基金/基金.rb'
require_relative '../../../../../../01-数据采集/处理逻辑/00-初始化/20-编码语言/Ruby/结果集.rb'

结果集 = ResultFile.new({文件路径: ARGV[0]})

基金对象 = {}
FundLib.new().文件夹.each do |代码, 路径|
  fund = Fund.new(fund_code: 代码, storage_path: 路径)
  基金对象[fund.基金简称] = fund
end

映射 = {
  "基金全称": {
    "类型" => "keyword"
  },
  "基金简称": {
    "类型" => "keyword"
  },
  "基金代码": {
    "类型" => "keyword"
  },
  "基金类型": {
    "类型" => "keyword"
  },
  "发行日期": {
    "类型" => "keyword",
  },
  "成立日期": {
    "类型" => "keyword",
  },
  "资产规模": {
    "类型" => "double",
    "字段名" => "资产规模(亿元)",
    "字段值" => lambda{|值| 值['数值']}
  }
}

基本信息 = {
  "映射" => {},
  "文档" => {},
}

基金对象.each do |名称, 对象|
  基金信息 = {}
  # 对象.instance_variables.each do |属性名|
  #   next if 属性名.to_s =~/^@[a-z A-z]+.*/
  #   基金信息["#{属性名.to_s.gsub('@', '')}"] = 对象.instance_variable_get(属性名)
  # end

  映射.each do |键, 定义|
    属性值 = 对象.instance_variable_get("@#{键}")
    基金信息["#{定义['字段名']||键}"] = 定义['字段值'] ? 定义['字段值'].call(属性值) : 属性值
  end

  基本信息['文档'][对象.基金代码.gsub(/(\d+).*/, '\1')] = 基金信息
end

mapping = {}
映射.each do |键, 定义|
  mapping["#{定义['字段名']||键}"] = {
    "type": 定义['类型']
  }
end

基本信息['映射'] = {"properties": mapping}

((结果集.内容['/']['ES文档']||={})['基金']||={})['基本信息'] = 基本信息.to_json
结果集.更新
