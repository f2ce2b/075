require_relative '../../../../../面向对象/基金/代码库.rb'
require_relative '../../../../../面向对象/基金/基金.rb'
require_relative '../../../../../../01-数据采集/处理逻辑/00-初始化/20-编码语言/Ruby/结果集.rb'
require_relative '../../../../../面向对象/ES/ES.rb'

结果集 = ResultFile.new({文件路径: ARGV[0]})
es = ES.new()

基金对象 = {}
FundLib.new().文件夹.each do |代码, 路径|
  fund = Fund.new(fund_code: 代码, storage_path: 路径)
  next if fund.换手率 == {}
  基金对象[代码] = fund
end

换手率 = {}
基金对象.each do |代码, 对象|
  对象.换手率.each do |时间,内容_|
    内容 = 内容_.call
    binding.pry unless 时间 == 内容['报告日期']
    文档 = {
      '基金简称': 对象.基金简称,
      '基金代码': 代码,
      '报告日期': 内容['报告日期'],
      '换手率': 内容['换手率'],
    }
    换手率[Digest::MD5.hexdigest(JSON.pretty_generate(文档))] = 文档
  end
end

历年数据定义 = {
  "基金简称": {
    "类型": "keyword"
  },
  "基金代码": {
    "类型": "keyword"
  },
  "报告日期": {
    "类型": "keyword",
  },
  "换手率": {
    "类型": "double"
  }
}

历年数据 = es.生成索引定义及文档(定义: 历年数据定义, 目标对象: 换手率)

(((结果集.内容['/']['ES文档']||={})['基金']||={})['换手率']||={})['历年数据'] = 历年数据.to_json


近期数据定义 = {
  "基金简称": {
    "类型": "keyword"
  },
  "基金代码": {
    "类型": "keyword"
  },
  "换手率": {
    # "类型": "double",
    "字段值": lambda do |**args|
      内容_ = args[:数据].values.last
      内容 = 内容_.call
      内容['换手率']
    end
  },
  "报告日期": {
    "类型": "keyword",
    # "类型": "date",
    # "格式": "yyyy-MM-dd",
    "字段值": lambda do |**args|
      内容_ = args[:数据].换手率.values.last
      内容 = 内容_.call
      内容['报告日期']
    end
  }
}

近期数据 = es.生成索引定义及文档(定义: 近期数据定义, 目标对象: 基金对象)

(((结果集.内容['/']['ES文档']||={})['基金']||={})['换手率']||={})['近期数据'] = 近期数据.to_json


换手率分析定义 = {
  "基金简称": {
    "类型": "keyword"
  },
  "基金代码": {
    "类型": "keyword"
  },
  "样本数量": {
    "类型": "double",
    "字段值": lambda {|**args|}
  },
  "平均值": {
    "类型": "double",
    "字段值": lambda {|**args|}
  },
  "最大值": {
    "类型": "double",
    "字段值": lambda {|**args|}
  },
  "最小值": {
    "类型": "double",
    "字段值": lambda {|**args|}
  },
  "标准差": {
    "类型": "double",
    "字段值": lambda {|**args|}
  },
  "装载": {
    "字段值": lambda do |**args|
      数据 = args[:数据].换手率.map{|k,v| [k, v.call['换手率']]}.to_h      
      样本 = 数据.values

      args[:文档]['样本数量'] = 样本.length
      args[:文档]['平均值'] = (样本.sum / 样本.length).round(4)
      args[:文档]['最大值'] = 样本.max
      args[:文档]['最小值'] = 样本.min

      mean = 样本.sum(0.0) / 样本.size
      sum = 样本.sum(0.0) { |element| (element - mean) ** 2 }
      variance = sum / (样本.size - 1)
      args[:文档]['标准差'] = Math.sqrt(variance).round(4)
      nil
    end
  }
}

换手率分析 = es.生成索引定义及文档(定义: 换手率分析定义, 目标对象: 基金对象)

(((结果集.内容['/']['ES文档']||={})['基金']||={})['换手率']||={})['换手率分析'] = 换手率分析.to_json

结果集.更新
