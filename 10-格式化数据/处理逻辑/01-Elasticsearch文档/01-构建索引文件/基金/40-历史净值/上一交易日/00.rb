require_relative '../../../../../../面向对象/基金/代码库.rb'
require_relative '../../../../../../面向对象/基金/基金.rb'
require_relative '../../../../../../../01-数据采集/处理逻辑/00-初始化/20-编码语言/Ruby/结果集.rb'
require_relative '../../../../../../面向对象/ES/ES.rb'

结果集 = ResultFile.new({文件路径: ARGV[0]})
es = ES.new()

基金对象 = {}
FundLib.new().文件夹.each do |代码, 路径|
  fund = Fund.new(fund_code: 代码, storage_path: 路径)
  next if fund.历史净值 == {}
  基金对象[代码] = fund
end

上一交易日定义 = {
  "基金简称": {
    "类型" => "keyword"
  },
  "基金代码": {
    "类型" => "keyword"
  },
  "净值日期": {
    "类型": "keyword",
    "字段值" => lambda {|**args|}
  },
  "单位净值": {
    "类型" => "double",
    "字段值" => lambda {|**args|}
  },
  "日增长率": {
    "类型" => "double",
    "字段值" => lambda {|**args|}
  },
  "装载": {
    "字段值" => lambda do |**args|
      净值 = args[:数据].历史净值.values.last.call()
      args[:文档]['净值日期'] = 净值['净值日期']
      args[:文档]['单位净值'] = 净值['单位净值'].to_f
      args[:文档]['日增长率'] = 净值['日增长率'].gsub('%', '').to_f
      args[:文档][:id] = Digest::MD5.hexdigest(JSON.pretty_generate(args[:文档]))
      nil
    end
  }
}

上一交易日 = es.生成索引定义及文档(定义: 上一交易日定义, 目标对象: 基金对象)

(((结果集.内容['/']['ES文档']||={})['基金']||={})['历史净值']||={})['上一交易日'] = 上一交易日.to_json

结果集.更新