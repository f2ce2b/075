require 'pry'
require 'json'
require_relative '../../../01-数据采集/处理逻辑/00-初始化/20-编码语言/Ruby/结果集.rb'

结果集 = ResultFile.new({文件路径: ARGV[0]})

def 层级迭代(哈希:, 键组: [])
  层级地址 = File.join(键组.flatten)
  Dir.mkdir(层级地址) unless File::directory?(层级地址)

  哈希.each do |键, 值|
    case 值
    when Hash
      层级迭代(哈希: 值, 键组: [键组, 键])
    when String
      File.open(File.join([层级地址, 键]), 'w') do |文件对象|
        begin
          文件对象.write(JSON.pretty_generate(JSON.parse(值)))  
        rescue => exception
          文件对象.write(值)
        end
      end
    end
  end
end

结果集.内容['/'].each do |键, 值|
  层级迭代(哈希: 值, 键组: [结果集.内容['路径']['数据存储']['格式数据'], 键])
end
