require 'pry'
require 'uri'
require 'json'
require 'rest-client'
require 'ruby-pinyin'
require_relative '../../01-数据采集/处理逻辑/00-初始化/20-编码语言/Ruby/结果集.rb'

class Dobject
  def 加载属性(地址:)
    Dir.foreach(地址).sort.each do |filename|
      next unless filename =~ /^\d{2}\-(.*)/
      
      属性名 = filename.gsub(/^\d{2}\-(.*)/,'\1')
      属性值 = File.open("#{地址}/#{filename}"){|对象| 内容 = 对象.read; JSON.parse(内容) rescue 内容}
      self.class.attr_accessor "#{属性名}"
      self.instance_variable_set("@#{属性名}", 属性值)
    end

  end

  def 加载方法(地址:)
    加载方法 = lambda do |文件夹, 路径=[]|
      Dir.foreach(文件夹).sort.each do |filename|
        next if ['.', '..'].include?(filename)
        path = File.join([路径, filename])
        if File.file?(path)
          res = require path
          # binding.pry unless res
        elsif File.directory?(path)
          加载方法.call(path, [路径, filename].flatten)
        end
      end
    end

    加载方法.call(地址, [地址])

    # 加载方法文件夹中子文件夹(子文件夹需定义为模块)
    Dir["#{地址}/*"].each do |文件夹|
      dir_name = 文件夹.split('/').last
      拼音 = PinYin.of_string(dir_name).join('_')
      模块名 = 拼音.split('_').collect!{|w| w.capitalize}.join
      self.class.include Module.const_get(模块名)
    end
  end

  def initialize(**params)
    [params[:属性]].flatten.each do |地址|
      raise "#{地址}地址不存在" unless File.directory?(地址)
      加载属性(地址: 地址)
    end

    [params[:方法]].flatten.each do |地址|
      raise "#{地址}地址不存在" unless File.directory?(地址)
      加载方法(地址: 地址)
    end
  end
end