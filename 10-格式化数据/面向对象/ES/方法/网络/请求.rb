module WangLuo

  DefaultPrintFormat = lambda do |args|
    print "[#{Time.now.strftime("%Y-%m-%d %H:%M:%S:%L")}] 开始请求: #{args[:链接]}\n"
    if args[:请求体]
      print "[#{Time.now.strftime("%Y-%m-%d %H:%M:%S:%L")}] 请求内容: #{args[:请求体].to_json}\n"
    end
  end

  def 请求(链接:, 请求方法: "Get", 请求超时: 15, 请求头: nil, 请求体: nil, 输出日志: DefaultPrintFormat)
    输出日志.call(**{链接: 链接, 请求体: 请求体, 请求方法: 请求方法})
    链接 = URI.parse(URI.escape(链接)).to_s
    
    # Rest-client 本身不支持 JSON, 因此在将负载传递给 Rest-client 之前将其序列化为字符串.
    请求体 = 请求体.is_a?(Hash) ? 请求体.to_json : 请求体

    请求参数 = {
      'url' => 链接, 
      'method' => 请求方法, 
      'headers' => 请求头, 
      'cookies' => nil, 
      'payload' => 请求体, 
      'timeout' => 请求超时, 
      'open_timeout' => 请求超时
    }

    请求参数.delete_if{|k,v| v.nil?}
    begin
      response = RestClient::Request.execute(**Hash[请求参数.map{|(k,v)| [k.to_sym,v]}])
    rescue RestClient::NotFound
      return [false, '返回404状态', 404]
    # rescue RestClient::NotAcceptable
    #   return [false, '返回406状态', 406]
    rescue => e
      binding.pry
      return [false, e.to_s]
    end

    return [false, '响应状态码异常', response] unless ([200, 201].include?(response.code))

    [true, (JSON.parse(response.body) rescue response.body), response]
  end
end