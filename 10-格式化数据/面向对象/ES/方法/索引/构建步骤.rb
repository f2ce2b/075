module SuoYin
  def 检查索引是否存在
    res = self.请求(链接: @main_index_url)
    @main_index_detail = res[1]
    [true, (res[0] ? '存在' : '不存在')]
  end
  
  def 子索引是否存在
    res = self.全部索引
    match_index = res.select{|index| index.include?(@index_md5) }
    return [true, (match_index.size>0 ? '是' : '否')]
  end

  def 删除所有子索引
    index_detail = @main_index_detail.clone
    index_detail.delete_if{|k, v| k == @子索引名}
    index_detail.each do |name, value|
      res = self.请求(链接: self.链接_es(name), 请求方法: "DELETE")
      binding.pry unless res[0]
    end
    [true, '']
  end

  def 创建子索引
    res = self.请求(链接: @node_index_url, 请求方法: "PUT")
    binding.pry unless res[0]
    res
  end

  def 子索引关联主索引
    self.请求(链接: "#{@node_index_url}/_alias/#{@主索引名}", 请求方法: "PUT")
  end

  def 定义索引
    res = self.请求(链接: "#{@node_index_url}/_mapping", 请求方法: "PUT", 请求头: {'content-type' => 'application/json'}, 请求体: @映射)
    binding.pry unless res[0]
    res
  end

  def 创建文档
    @文档.each do |id, 内容|
      res = self.请求(链接: "#{@node_index_url}/_doc/#{id}", 请求方法: "POST", 请求头: {'content-type' => 'application/json'}, 请求体: 内容)
      binding.pry unless res[0]
    end
    [true, '']
  end

  def 内容是否完全相同
    res = self.全部索引
    match_index = res.select{|index| index.include?(@doc_md5) }
    return [true, (match_index.size>0 ? '是' : '否')]
  end

  def 筛选缺失文档创建
    res = self.全部索引
    match_indices = res.select{|index| index.include?(@index_md5) }
    binding.pry if match_indices.size != 1
    @node_index_old_name = match_indices[0]
    res = self.所有文档ID(名称: @node_index_old_name)
    binding.pry if res[1] == @文档.keys
    
    缺失文档id = @文档.keys - res[1]
    缺失文档id.each do |id|
      res = self.新建文档(索引名: @node_index_old_name, id: id, 内容: @文档[id])
      binding.pry unless res[0]
    end

    [true, '']
  end
  
  def 筛选多余文档删除
    res = self.所有文档ID(名称: @node_index_old_name)
    多余文档id = res[1] - @文档.keys

    多余文档id.each do |id|
      self.删除匹配文档(名称: @node_index_old_name, 匹配条件: {"match": {"id": id}})
    end

    [true, '']
  end

  def 重新进行索引
    self.重新索引(旧名称: @node_index_old_name, 新名称: @子索引名)
    [true, '']
  end

  def 删除旧索引
    self.删除索引(名称: @node_index_old_name)
    [true, '']
  end

end
