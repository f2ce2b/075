module SuoYin

  def 生成索引定义及文档(定义:, 目标对象:)
    基本信息 = {
      "映射" => {},
      "文档" => {},
    }
    
    目标对象.each do |id, 对象|
      对象文档 = {}
      定义.each do |键, 字段定义|
        case 对象
        when Hash
          binding.pry unless 对象.keys.map(&:to_s) == 定义.keys.map(&:to_s)
          属性值 = 对象[键] || 对象[键.to_s]|| 对象[键.to_sym] || binding.pry
        else
          属性值 = 对象.instance_variable_get("@#{键}") || (对象.respond_to?(键) ? 对象.send(键) : 对象)
        end

        对象文档["#{字段定义['字段名']||键}"] = (字段值=字段定义['字段值']||字段定义[:字段值]) ? 字段值.call(数据: 属性值, 文档: 对象文档) : 属性值
      end

      id = 对象文档[:id] || 对象文档['id'] || id
      keys = [定义.keys.map(&:to_s), 定义.keys.map(&:to_sym)].flatten
      基本信息['文档'][id] = 对象文档.delete_if{|k,v| v.nil? || !keys.include?(k)}
    end
    

    字典 = {
      'type': '类型',
      'format': '格式',
    }
    字典 = 字典.map{|k, v| [[v.to_sym,k], [v.to_s,k]].to_h}.reduce({}, :merge)

    mapping = {}
    定义.each do |键, 字段定义|
      map = mapping["#{字段定义['字段名']||键}"] = {}
      字典.each do |键,值|
        next unless 字段定义[键]
        map[值] = 字段定义[键]
      end
      binding.pry if map.values.include?(nil)
    end

    基本信息['映射'] = {"properties": mapping.delete_if{|k,v| v == {}}}
    
    return 基本信息
  end
end