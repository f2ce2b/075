module LiuCheng

  def 跳出流程
    [false, 200]
  end

  def 执行流程(流程:, 对象: )
    迭代子流程 = lambda do |子流程:, 键组:|
      print "[#{Time.now.strftime("%Y-%m-%d %H:%M:%S:%L")}] 开始执行: #{键组.join(' -> ')}\n"
      if self.respond_to?(键组.last)
        res = self.send(键组.last)
        raise '返回结果为空' if res.nil?
        return res unless res[0] 
        (子流程||={}).delete_if{|k,v| ![res[1]].include?(k.to_s)}
      end

      case 子流程
      when Hash
        子流程.each do |键, 值|
          res = 迭代子流程.call(子流程: 值, 键组:[键组, 键].flatten)
          return res unless res[0]
        end
      when Array, String
        [子流程].flatten.each do |流程名|
          res = 迭代子流程.call(子流程: nil, 键组:[键组, 流程名].flatten)
          return res unless res[0]
        end
      else
        binding.pry
      end
      
      [true, '']
    end
    
    流程.each do |键, 值|
      res = 迭代子流程.call(子流程: 值.clone, 键组:[键])
      return res unless res[0]
    end

    [true, '']
  end
end