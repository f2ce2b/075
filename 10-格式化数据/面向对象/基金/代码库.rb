require 'json'
require 'pry'
require_relative '../../../01-数据采集/处理逻辑/00-初始化/20-编码语言/Ruby/结果集.rb'

结果集 = ResultFile.new({文件路径: ARGV[0]})

数据结构目录 = 结果集.内容['路径']['数据存储']['结构数据']

STORAGE_PATH = Dir["#{数据结构目录}/[0-9]*-基金/00-代码库"][0]
unless STORAGE_PATH && File::directory?(STORAGE_PATH)
  print "[false, \"目录不存在\"]\n"
  exit 255
end

class FundLib
  attr_reader :storage_path

  def initialize(**params)
    @storage_path = STORAGE_PATH
  end

  def 文件夹
    文件夹数组 = Dir["#{@storage_path}/[0-9]*"].sort
    文件夹键值对 = {}
    
    文件夹数组.each do |路径|
      文件夹键值对[路径.split('/')[-1]] = 路径
    end

    return 文件夹键值对
  end
end
