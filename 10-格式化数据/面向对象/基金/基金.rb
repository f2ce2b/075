require 'json'
require 'pry'

class Fund
  attr_accessor :fund_code
  attr_reader :storage_path

  def initialize(**params)
    @fund_code = params[:fund_code]
    @storage_path = params[:storage_path]

    Dir.foreach(self.storage_path).sort.each do |filename|
      next unless filename =~ /^\d{2}\-(.*)/

      文件路径 = File::join([self.storage_path, filename])
      属性名 = filename.gsub(/^\d{2}\-(.*)/,'\1')
      if File::file?(文件路径)
        属性值 = nil
        File::open(文件路径) do |文件对象|
          属性值 = 文件对象.read
          属性值 = (JSON::parse(属性值) rescue 属性值)
        end
        self.class.attr_accessor 属性名
        self.instance_variable_set("@#{属性名}", 属性值)
      elsif File::directory?(文件路径)
        next if self.respond_to?(属性名)
        # result = self.class.迭代目录("#{self.storage_path}/#{filename}")
        self.class.define_method 属性名 do
          return self.class.迭代目录("#{self.storage_path}/#{filename}")
          # return result
        end

      end
    end

  end


  def self.迭代目录(路径, 上级目录=[])
    结果集 = {}
    子文件夹 = Dir["#{路径}/*"].sort_by{|x| x.split('/')[-1].to_i }

    if Dir["#{路径}/*/*"].size >0
      子文件夹.each do |子路径|
        结果集.merge!(self.迭代目录(子路径, [上级目录, 子路径.split('/')[-1]].flatten))
      end
    elsif Dir["#{路径}/*"].size >0
      排序后 = nil
      哈希键 = lambda{|文件名|[上级目录, 文件名].flatten.join('/')}
      case 子文件夹[0]
      when /.*\/\d{4}-\d{2}-\d{2}/
        排序后 = 子文件夹.sort_by{|x| x.split('/')[-1].split('-')[1].to_i }
      when /.*\/\d{2}-\d{2}/
        排序后 = 子文件夹.sort_by do |x| 
          array = x.split('/')[-1].split('-')
          [array[0].to_i, array[1].to_i]
        end
        哈希键 = lambda{|文件名|[上级目录, 文件名].flatten.join('-')}
      else
        排序后 = 子文件夹
      end
      
      排序后.each do |文件地址|
        文件名 = 文件地址.split('/')[-1]
        结果集[哈希键.call(文件名)] = lambda do 
          File::open(文件地址)do |文件对象| 
            内容 = 文件对象.read
            JSON::parse(内容) rescue 内容
          end
        end
      end
    else
      return {}
    end

    结果集
  end

end